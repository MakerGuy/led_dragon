
// ---------------- BLE Module Setup --------------------- //
#include <string.h>
#include <Arduino.h>
#if not defined (_VARIANT_ARDUINO_DUE_X_) && not defined (_VARIANT_ARDUINO_ZERO_)
  #include <SoftwareSerial.h>
#endif

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_UART.h"
#include "BluefruitConfig.h"

#define FACTORYRESET_ENABLE         0
#define MINIMUM_FIRMWARE_VERSION    "0.6.6"
#define MODE_LED_BEHAVIOUR          "MODE"

SoftwareSerial bluefruitSS = SoftwareSerial(BLUEFRUIT_SWUART_TXD_PIN, BLUEFRUIT_SWUART_RXD_PIN);

Adafruit_BluefruitLE_UART ble( bluefruitSS, 
                               BLUEFRUIT_UART_MODE_PIN,
                               BLUEFRUIT_UART_CTS_PIN, 
                               BLUEFRUIT_UART_RTS_PIN);

// function prototypes over in packetparser.cpp
uint8_t readPacket(Adafruit_BLE *ble, uint16_t timeout);
float parsefloat(uint8_t *buffer);
void printHex(const uint8_t * data, const uint32_t numBytes);

char command[65] = "sliding_rainbow\0";

// ---------------- FastLED Setup --------------------- //
#include "FastLED.h"
FASTLED_USING_NAMESPACE

#define NUM_SPINE_LEDS 21 
#define NUM_TALON_LEDS 12
#define NUM_HEAD_LEDS 7

#define LEFT_HORN    0
#define RIGHT_HORN   1
#define RIGHT_EYE    2
#define LEFT_EYE     3
#define LEFT_TOOTH   4
#define RIGHT_TOOTH  5
#define BOTTOM_TEETH 6

#define SPINE_PIN 4
#define TALON_PIN 5
#define HEAD_PIN  6

CRGB spine_leds[NUM_SPINE_LEDS];
CRGB talon_leds[NUM_TALON_LEDS];
CRGB head_leds [NUM_HEAD_LEDS];

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

#define  FRAMES_PER_SECOND 150
uint16_t brightness      = 120;
uint8_t  gHue            = 0; // Changes periodically but not tied to a tempo
uint8_t  slidingHue      = 0;
uint8_t  slowWalkHue     = 0;
uint8_t  bpmHue          = 0; // Should only change at a certain bpm. Used in bpm_flash only right now
uint8_t  bpm             = 128; // Default bpm for animations
uint8_t  safe_bpm        = 1; //Whether the animation will scale down speed to avoid epileptic risk
uint8_t  thisBeat        = 0;
uint8_t  lastBeat        = 0;
uint8_t  cycleIndex      = 0;
uint8_t  blink_talons    = 0; //Cycle blinked talons in certain animations

// TODO
//  - Talon pinwheels - Talons flash in circles per foot changing colors
//  - 

// A small helper
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}

void setup() {
  Serial.begin(115200);
  LEDS.addLeds<WS2812B,SPINE_PIN,GRB>(spine_leds,NUM_SPINE_LEDS);
  LEDS.addLeds<WS2812B,TALON_PIN,GRB>(talon_leds,NUM_TALON_LEDS);
  LEDS.addLeds<WS2812B,HEAD_PIN,GRB> (head_leds,NUM_HEAD_LEDS);
  LEDS.setBrightness(brightness);
  LEDBootup();

  /* Initialise the module */
  Serial.print(F("Initialising the Bluefruit LE module: "));
  
  if ( !ble.begin(VERBOSE_MODE) ) { error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?")); }
  Serial.println( F("OK!") );
  
  if ( FACTORYRESET_ENABLE )
  {
    /* Perform a factory reset to make sure everything is in a known state */
    Serial.println(F("Performing a factory reset: "));
    if ( ! ble.factoryReset() ){
      error(F("Couldn't factory reset"));
    }
  }

  /* Disable command echo from Bluefruit */
  ble.echo(false);

  Serial.println("Requesting Bluefruit info:");
  /* Print Bluefruit information */
  ble.info();

  Serial.println(F("Please use Adafruit Bluefruit LE app to connect in Controller mode"));
  Serial.println();

  ble.verbose(false);  // debug info is a little annoying after this point!

/*
  // Wait for connection
  while (! ble.isConnected()) {
      head_leds[LEFT_EYE] = CRGB::Red;
      head_leds[RIGHT_EYE] = CRGB::Red;
      FastLED.show();
      delay(200);
  }

  head_leds[LEFT_EYE] = CRGB::Purple;
  head_leds[RIGHT_EYE] = CRGB::Purple;
  FastLED.show();
  
  Serial.println(F("******************************"));

  // LED Activity command is only supported from 0.6.6
  if ( ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION) )
  {
    // Change Mode LED Activity
    Serial.println(F("Change LED activity to " MODE_LED_BEHAVIOUR));
    ble.sendCommandCheckOK("AT+HWModeLED=" MODE_LED_BEHAVIOUR);
  }

  // Set Bluefruit to DATA mode
  Serial.println( F("Switching to DATA mode!") );
  ble.setMode(BLUEFRUIT_MODE_DATA);

  Serial.println(F("******************************"));

  head_leds[LEFT_EYE] = CRGB::Blue;
  head_leds[RIGHT_EYE] = CRGB::Blue;
  FastLED.show();
*/
}

void printCommands() {
  ble.print("AT+BLEUARTTX=");
  ble.println("**Command List**\\n"
              "rainbow_erase\\n"
              "sliding_rainbow\\n"
              "color_pile\\n"
              "bpm_flash\\n"
              "walk_pulse\\n"
              "slow_walk\\n"
              "equalizer\\n"
              "set_distraction:1-3\\n"
              "cycle\\n"
              "off\\n"
              "help\\n\\n");
  ble.waitForOK();
  ble.print("AT+BLEUARTTX=");
  ble.println("**Settings**\\n"
              "delay:int\\n"
              "max_brightness:0-255\\n"
              "blink_talons:0|1\\n"
              "bpm:int\\n"
              "safe_bpm:0|1\\n"
              "skip\\n");
  ble.waitForOK();
}

typedef void (*AnimationsList[])();
AnimationsList gAnimations = { slidingRainbow, 
                               walk_pulse,
                               colorPile, 
                               equalizer,
                               slow_walk,
                               bpm_flash };

void loop()
{
  if(! ble.isConnected()) {
    head_leds[LEFT_EYE] = CRGB::Red;
    head_leds[RIGHT_EYE] = CRGB::Red;
    FastLED.show();
  }
  else {
    if ( ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION) )
    {
      // Change Mode LED Activity
      //Serial.println(F("Change LED activity to " MODE_LED_BEHAVIOUR));
      ble.sendCommandCheckOK("AT+HWModeLED=" MODE_LED_BEHAVIOUR);
    }
  
    // Set Bluefruit to DATA mode
    //Serial.println( F("Switching to DATA mode!") );
    ble.setMode(BLUEFRUIT_MODE_DATA);
    head_leds[LEFT_EYE] = CRGB::Blue;
    head_leds[RIGHT_EYE] = CRGB::Blue;
    FastLED.show();
  }

  char oldCmd[65];
  strcpy(oldCmd, command);
  
  // Check for incoming characters from Bluefruit
  ble.println("AT+BLEUARTRX");
  ble.readline();
  if (strcmp(ble.buffer, "OK") == 0) {
    // no data
    //return;
  }
  else if(strcmp(ble.buffer, command) != 0) {
    char formattedCmd[50];
    sprintf(formattedCmd, "%X\n", ble.buffer);
    //Serial.print(F("InBuffer: ")); Serial.println(formattedCmd);
    strcpy(command, ble.buffer);
  }

  char splittableCmd[65];
  strcpy(splittableCmd, command);
  
  //ble.waitForOK();
  
  char *animation  = strtok(splittableCmd, ":");
  char *parameters = strtok(NULL, ":");

  // Adafruit app sends arbitrary colors in this format
  if(splittableCmd[0] == '!' && splittableCmd[1] == 'C') {
    // Something went wrong and checksum fails. Default to last command
    if (strlen(splittableCmd) != 6) {
      strcpy(command, oldCmd);
    }
    else {
      animation = "fill_color";
      parameters = &splittableCmd[2];
      /*
      char formattedCmd[50];
      sprintf(formattedCmd, "r: %-3X\tg: %-3X\tb: %-3X\n", splittableCmd[2], splittableCmd[3], splittableCmd[4]);    
      Serial.print(F("Formatted: ")); Serial.print(formattedCmd);
      */
      //Serial.print(F("\tRed: ")); Serial.print(parameters[0], HEX);
      //Serial.print(F("\tGreen: ")); Serial.print(parameters[1], HEX);
      //Serial.print(F("\tBlue: ")); Serial.print(parameters[2], HEX);
      //Serial.print(F("\tRaw size: ")); Serial.println(strlen(parameters));
    }
  }
  else
  {
    Serial.print(F("Animation: ")); Serial.print(animation);
    Serial.print(F("\tParameters: ")); Serial.println(parameters);
  }
  
  if (strcmp(animation, "max_brightness") != 0 &&
      strcmp(animation, "delay") != 0 &&
      strcmp(animation, "bpm") != 0 &&
      strcmp(animation, "safe_bpm") != 0 &&
      strcmp(animation, "blink_talons") != 0 &&
      strcmp(animation, "skip") != 0 &&
      strcmp(animation, "help") != 0 &&
      strcmp(command, oldCmd) != 0)
    {
      clearLEDS();
      LEDS.setBrightness(brightness);
    }
    
  if(strcmp(animation, "rainbow_erase") == 0) {
    rainbowErase();
  }
  else if (strcmp(animation, "sliding_rainbow") == 0) {
    slidingRainbow();
  }
  else if (strcmp(animation, "color_pile") == 0) {
    colorPile();
  }
  else if (strcmp(animation, "bpm_flash") == 0) {
    bpm_flash();
  }
  else if (strcmp(animation, "equalizer") == 0) {
    equalizer();
  }
  else if (strcmp(animation, "slow_walk") == 0) {
    slow_walk();
  }
  else if (strcmp(animation, "fill_color") == 0) {
    uint8_t r = parameters[0];
    uint8_t g = parameters[1];
    uint8_t b = parameters[2];
    fill_color(r,g,b);
  }
  else if (strcmp(animation, "set_distraction") == 0) {
    uint8_t dLevel = (uint8_t) atoi(parameters);
    set_distraction(dLevel);
  }
  else if (strcmp(animation, "cycle") == 0) {
    //Serial.print(F("Animation Index: ")); Serial.println(cycleIndex);
    gAnimations[cycleIndex]();
  }
  else if (strcmp(animation, "walk_pulse") == 0) {
    walk_pulse();
  }
  else if (strcmp(animation, "slow_walk") == 0) {
    slow_walk();
  }
  // Settings
  else if (strcmp(animation, "max_brightness") == 0) {
    brightness = atoi(parameters);
    LEDS.setBrightness(brightness);
    strcpy(command, oldCmd);
  }
  else if (strcmp(animation, "blink_talons") == 0) {
    blink_talons = (uint8_t) atoi(parameters);
    strcpy(command, oldCmd);
  }
  else if (strcmp(animation, "delay") == 0) {
    delay(atoi(parameters));
    strcpy(command, oldCmd);
  }
  else if (strcmp(animation, "bpm") == 0) {
    bpm = (uint8_t) atoi(parameters);
    strcpy(command, oldCmd);
  }
  else if (strcmp(animation, "safe_bpm") == 0) {
    safe_bpm = (uint8_t) atoi(parameters);
    strcpy(command, oldCmd);
  }
  else if (strcmp(animation, "skip") == 0) {
    nextCycleIndex();
    strcpy(command, oldCmd);
  }
  else if (strcmp(animation, "off") == 0) {
    return;
  }
  else if (strcmp(animation, "help") == 0) {
    printCommands();
    strcpy(command, oldCmd);
  }
  else {
    strcpy(command, oldCmd);
  }

  EVERY_N_MILLISECONDS( 20 ) { gHue+=10; }
  EVERY_N_SECONDS( 600 ) { nextCycleIndex(); }
  
}

void nextCycleIndex() {
  cycleIndex = (cycleIndex + 1) % ARRAY_SIZE(gAnimations);
}

// Flash a single color for a specified amount of time
void flashColor(CRGB led_arr[], CRGB color, uint16_t arrIndex, uint16_t flashDelay)
{
    CRGB black = {.r = 0, .g = 0,   .b = 0};
    led_arr[arrIndex] = color;
    FastLED.show();
    delay(flashDelay);
    led_arr[arrIndex] = black;
    FastLED.show();  
}

// Turn off all LEDs
void clearLEDS()
{
  fadeToBlackBy(head_leds,  NUM_HEAD_LEDS,  255);
  fadeToBlackBy(spine_leds, NUM_SPINE_LEDS, 255);
  fadeToBlackBy(talon_leds, NUM_TALON_LEDS, 255);
  FastLED.show();
}

// Bootup sequence to verify all LEDS work
void LEDBootup() 
{
  uint16_t flashSpeed      = 60;
  uint16_t colorCycleSpeed = 10;
  CRGB bootColor = {.r = 255, .g = 255, .b = 255};
  CRGB black = {.r = 0, .g = 0,   .b = 0};

  // Flash each LED green to verify they work
  for( uint16_t i = 0; i < NUM_HEAD_LEDS;  i++) { flashColor(head_leds,  bootColor, i, flashSpeed); }
  for( uint16_t i = 0; i < NUM_SPINE_LEDS; i++) { flashColor(spine_leds, bootColor, i, flashSpeed); }
  for( uint16_t i = 0; i < NUM_TALON_LEDS; i++) { flashColor(talon_leds, bootColor, i, flashSpeed); }

  // Cycle all LEDs through rainbow to verify RGB's are working
  CHSV hsv = {.hue = 0, .val = 255, .sat = 240};
  for( uint8_t i = 0; i < 255; ++i ) 
  {
    for( uint8_t j = 0; j < NUM_HEAD_LEDS; ++j)  { hsv2rgb_spectrum( hsv, head_leds[j]  ); }
    for( uint8_t j = 0; j < NUM_SPINE_LEDS; ++j) { hsv2rgb_spectrum( hsv, spine_leds[j] ); }
    for( uint8_t j = 0; j < NUM_TALON_LEDS; ++j) { hsv2rgb_spectrum( hsv, talon_leds[j] ); }
    hsv.hue = i;
    FastLED.show();
    delay(colorCycleSpeed);
  }

  // Turn LEDs off
  for( uint16_t i = 0; i < NUM_HEAD_LEDS; ++i)  { head_leds[i] = black; }
  for( uint16_t i = 0; i < NUM_SPINE_LEDS; ++i) { spine_leds[i] = black; }
  for( uint16_t i = 0; i < NUM_TALON_LEDS; ++i) { talon_leds[i] = black; }
  FastLED.show();
}

// Fade the array of leds to black from top to bottom, at the given speed
void eraseFadeDown(CRGB led_arr[], uint16_t num_leds, uint16_t erase_speed) 
{
  for( uint16_t i = 0; i < num_leds + 2; ++i) 
  { 
    if(i > -1 && i < num_leds)     { led_arr[i].nscale8(150); }
    if(i > 0  && i < num_leds + 1) { led_arr[i-1].nscale8(75); }
    if(i > 1  && i < num_leds + 2) { led_arr[i-2].nscale8(0); }
    delay(erase_speed);
    FastLED.show();
   }
}

// Light up all leds in rainbow, then fade them to black from top to bottom
void rainbowErase() 
{
  uint8_t hue = 0;
  uint16_t chaseSpeed = 50;
  fill_rainbow(head_leds, NUM_HEAD_LEDS, hue, 10);
  fill_rainbow(spine_leds, NUM_SPINE_LEDS, hue, 10);
  fill_rainbow(talon_leds, NUM_TALON_LEDS, hue, 10);
  FastLED.show();
  eraseFadeDown(head_leds, NUM_HEAD_LEDS, chaseSpeed);
  eraseFadeDown(talon_leds, NUM_TALON_LEDS, chaseSpeed);
  eraseFadeDown(spine_leds, NUM_SPINE_LEDS, chaseSpeed);
}

// Light up all LEDs in a rainbow, and then cycle them through the colors.
// Also turns off one talon at a time to add interest
void slidingRainbow() 
{
  uint8_t blackedTalon = 0;
  fill_rainbow(spine_leds, NUM_SPINE_LEDS, slidingHue, 5);
  fill_rainbow(talon_leds, NUM_TALON_LEDS, slidingHue, 5);
  for(uint8_t j = 0; j < NUM_HEAD_LEDS; ++j) {
    if(j != LEFT_EYE && j != RIGHT_EYE) {
      head_leds[j].setHue(slidingHue);
    }
  }
  if(blink_talons) { talon_leds[ blackedTalon ] = CRGB::Black; }
  FastLED.show();
  delay(500);
  slidingHue += 1;
  if (slidingHue == 256) { slidingHue = 0; }
  if ( slidingHue % 2 == 0 && slidingHue > 0 ) { blackedTalon++; }
  if ( blackedTalon == NUM_TALON_LEDS ) { blackedTalon = 0; }
}

// Light up talons and head in rainbow. Slowly pile up colors at the tail tetris style
void colorPile() 
{
  uint8_t hue = random8();
  uint8_t talonHue = random8();
  uint8_t delayVal = 35;
  FastLED.show();
  for( uint16_t i = NUM_SPINE_LEDS; i > 0; --i)
  {
    for( uint16_t j = 0; j < i; ++j)
    {
      fadeToBlackBy(spine_leds, i-1, 255);
      spine_leds[j].setHue(hue);
      fill_rainbow(talon_leds, NUM_TALON_LEDS, talonHue, 5);
      for(uint8_t j = 0; j < NUM_HEAD_LEDS; ++j) {
        if(j != LEFT_EYE && j != RIGHT_EYE) {
          head_leds[j].setHue(hue);
        }
      }
      FastLED.show();
      talonHue+=5;
      delay(delayVal);
    }
    hue += 10;
  }
  delay(500);
  for( uint16_t i = 0; i < NUM_SPINE_LEDS; ++i)
  {
    spine_leds[i] = CRGB::Black;
    FastLED.show();
    delay(50);
  }
  delay(300);
  
}

// Flashes all LEDs to the global bpm
void bpm_flash() {  
  CHSV hsv = {.hue = bpmHue, .val = 255, .sat = 240};
  uint8_t bright = beatsin8( bpm /*BPM*/, 20 /*dimmest*/, brightness /*brightest*/ );
  if( bright > (brightness -10) && bright < brightness)
  {
    bpmHue+=10;
    hsv.hue = bpmHue;
  }
  fill_solid(spine_leds,NUM_SPINE_LEDS, hsv); 
  hsv.hue = bpmHue + 128;
  fill_solid(talon_leds,NUM_TALON_LEDS, hsv);
  for(uint8_t i = 0; i < NUM_HEAD_LEDS; ++i) {
    if(i != LEFT_EYE && i != RIGHT_EYE) {
      head_leds[i] = hsv;
    }
  }
  FastLED.setBrightness( bright );
  FastLED.show();
}

// Fills head and talons with solid color.
// Sends a yo-yo ing rainbow down the tail that bounces to the global bpm
void equalizer() {
  uint8_t startHue = random8();
  uint8_t baseHeight = 0;
  uint8_t downSpeed  = 3;
  uint8_t upSpeed    = 6;
  fill_rainbow(spine_leds, baseHeight, startHue, 5);
  fill_rainbow(talon_leds, NUM_TALON_LEDS, startHue, 5);
  for(uint8_t i = 0; i < NUM_HEAD_LEDS; ++i) {
    if(i != LEFT_EYE && i != RIGHT_EYE) {
      head_leds[i].setHue(startHue);
    }
  }
  uint8_t continueHue = startHue + (5*baseHeight);
  uint8_t local_bpm = bpm;
  if(safe_bpm) {
    local_bpm = bpm/2;
    downSpeed = 10;
    upSpeed   = 15;
  }
  uint8_t beatWave = beat8( local_bpm ); 
  while (beatWave < 254) {
    beatWave = beat8( local_bpm );
  }
  uint8_t talonIndex = 0;
  for(uint8_t i = baseHeight; i < NUM_SPINE_LEDS; ++i) {
    spine_leds[i].setHue(continueHue);
    FastLED.show();
    continueHue = continueHue+5;
    delay(downSpeed);
  }
  Serial.println(talonIndex);
  for(uint8_t i = NUM_SPINE_LEDS; i > baseHeight; --i) {
    spine_leds[i-1] = CRGB::Black;
    FastLED.show();
    delay(upSpeed);
  }
}

// Lights up talons as a rainbow, head as a solid color.
// Light up spine in rainbow, and flashes a white pulse down it at the global bpm
void walk_pulse() {
  uint8_t startHue = gHue;
  uint8_t pulseSpeed  = 10;
  uint8_t hueIncrement = 10;
  // Fill head, spine, and tail with rainbow
  fill_rainbow(spine_leds, NUM_SPINE_LEDS, startHue, hueIncrement);
  fill_rainbow(talon_leds, NUM_TALON_LEDS, startHue, hueIncrement*1.5);
  for(uint8_t i = 0; i < NUM_HEAD_LEDS; ++i) {
    if(i != LEFT_EYE && i != RIGHT_EYE) {
      head_leds[i].setHue(gHue);
    }
  }
  FastLED.show();

  // Flash half speed if safety flag is on
  uint8_t local_bpm = bpm;
  if(safe_bpm) {
    local_bpm = bpm/2;
    pulseSpeed = 30;
  }

  // Get current place in the beat, and wait for downbeat if not already there
  uint8_t beatWave = beat8( local_bpm ); 
  while (beatWave < 254) {
    beatWave = beat8( local_bpm );
  }
  
  CHSV pulseColor = {.hue = startHue, .sat = 0, .val = 255};
  for(uint8_t i = 0; i < NUM_SPINE_LEDS; ++i) { 
    spine_leds[i] = pulseColor;
    if(i > 0) {
      pulseColor.sat = 50;
      spine_leds[i-1] = pulseColor;
    }
    if(i > 1) {
      pulseColor.sat = 75;
      spine_leds[i-2] = pulseColor;
    }
    if(i > 2) {
      spine_leds[i-3].setHue(startHue+(hueIncrement*(i-3)));
    }
    
    FastLED.show();
    pulseColor.hue += hueIncrement;
    delay(pulseSpeed);
  }
}

void slow_walk() {
  uint8_t startHue = random8();
  uint8_t pulseSpeed  = 160;
  uint8_t hueIncrement = 10;
  uint8_t trainLength = 7;
  fill_rainbow(talon_leds, NUM_TALON_LEDS, gHue, 10);
  for(uint8_t j = 0; j < NUM_HEAD_LEDS; ++j) {
    if(j != LEFT_EYE && j != RIGHT_EYE) {
      head_leds[j].setHue(gHue);
    }
  }

  CHSV pulseColor = {.hue = startHue, .sat = 255, .val = 255};
  for(uint8_t i = 0; i < NUM_SPINE_LEDS + trainLength; ++i) { 
    fadeToBlackBy(spine_leds, NUM_SPINE_LEDS, 255);
    if(i < NUM_SPINE_LEDS) {
      spine_leds[i] = CHSV(pulseColor.hue, pulseColor.sat, pulseColor.val - 192);
    }
    if(i > 0 && i-1 < NUM_SPINE_LEDS) {
      spine_leds[i-1] = CHSV(pulseColor.hue - hueIncrement, pulseColor.sat, pulseColor.val - 128); 
    }
    if(i > 1 && i-2 < NUM_SPINE_LEDS) {
      spine_leds[i-2] = CHSV(pulseColor.hue - (hueIncrement*2), pulseColor.sat, pulseColor.val - 64);
    }
    if(i > 2 && i-3 < NUM_SPINE_LEDS) {
      spine_leds[i-3] = CHSV(pulseColor.hue - (hueIncrement*3), pulseColor.sat, pulseColor.val);
    }
    if(i > 3 && i-4 < NUM_SPINE_LEDS) {
      spine_leds[i-4] = CHSV(pulseColor.hue - (hueIncrement*4), pulseColor.sat, pulseColor.val - 64);
    }
    if(i > 4 && i-5 < NUM_SPINE_LEDS) {
      spine_leds[i-5] = CHSV(pulseColor.hue - (hueIncrement*5), pulseColor.sat, pulseColor.val - 128);
    }
    if(i > 5 && i-6 < NUM_SPINE_LEDS) {
      spine_leds[i-6] = CHSV(pulseColor.hue - (hueIncrement*6), pulseColor.sat, pulseColor.val - 192);
    }
    
    FastLED.show();
    pulseColor.hue += hueIncrement;
    delay(pulseSpeed);
  }
  delay(1000);
}

void set_distraction(uint8_t level) {
  switch(level) {
    case 1:
      fill_solid(head_leds, NUM_HEAD_LEDS, CRGB::Green);
      fill_solid(spine_leds, NUM_SPINE_LEDS, CRGB::Green);
      fill_solid(talon_leds, NUM_TALON_LEDS, CRGB::Green);
      break;
    case 2:
      fill_solid(head_leds, NUM_HEAD_LEDS, CRGB::Gold);
      fill_solid(spine_leds, NUM_SPINE_LEDS, CRGB::Gold);
      fill_solid(talon_leds, NUM_TALON_LEDS, CRGB::Gold);
      break;
    case 3:
      fill_solid(head_leds, NUM_HEAD_LEDS, CRGB::Red);
      fill_solid(spine_leds, NUM_SPINE_LEDS, CRGB::Red);
      fill_solid(talon_leds, NUM_TALON_LEDS, CRGB::Red);
      break;
  }
}

void fill_color(uint8_t r, uint8_t g, uint8_t b) {
  if(r >= 0 && r <= 255 && g >= 0  && g <= 255 && b >= 0 && b <=255) { 
    fill_solid(head_leds, NUM_HEAD_LEDS, CRGB(r, g, b));
    fill_solid(spine_leds, NUM_SPINE_LEDS, CRGB(r, g, b));
    fill_solid(talon_leds, NUM_TALON_LEDS, CRGB(r, g, b));
  }
}

